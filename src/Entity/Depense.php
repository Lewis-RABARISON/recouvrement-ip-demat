<?php

namespace App\Entity;

use App\Repository\DepenseRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DepenseRepository::class)
 */
class Depense
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $motifs;

    /**
     * @ORM\Column(type="float")
     */
    private $monDep;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $preuve;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="depenses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $agent;

    public function __construct()
    {
        $this->date=new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMotifs(): ?string
    {
        return $this->motifs;
    }

    public function setMotifs(string $motifs): self
    {
        $this->motifs = $motifs;

        return $this;
    }

    public function getMonDep(): ?float
    {
        return $this->monDep;
    }

    public function setMonDep(float $monDep): self
    {
        $this->monDep = $monDep;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPreuve()
    {
        return $this->preuve;
    }

    public function setPreuve($preuve): self
    {
        $this->preuve = $preuve;

        return $this;
    }

    public function getAgent(): ?User
    {
        return $this->agent;
    }

    public function setAgent(?User $agent): self
    {
        $this->agent = $agent;

        return $this;
    }
}
