<?php

namespace App\Controller\FrontOffice;

use App\Repository\RechargementRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CourseController extends AbstractController
{
    /**
     * @Route("/course", name="course")
     * @IsGranted("ROLE_AGENT", message="Vous ne pouvez pas accéder sur cette url, sera réserve à l’agent!")
     */
    public function index(RechargementRepository $rechargementRepository): Response
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        $user = $this->getUser();
        
        // dd($rechargementRepository->CompteurRechargement());
        return $this->render('FrontOffice/course/index.html.twig', [
            'compteurs' => $rechargementRepository->CompteurRechargement($user),
        ]);
    }
}
