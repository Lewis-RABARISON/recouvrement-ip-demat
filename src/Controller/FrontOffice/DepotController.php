<?php

namespace App\Controller\FrontOffice;

use App\Entity\Depot;
use App\Entity\Soldes;
use App\Form\DepotType;
use App\Repository\DepotRepository;
use App\Repository\SoldesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DepotController extends AbstractController
{

    /**
     * @Route("/faire-depot", name="montant_depot")
     * @IsGranted("ROLE_AGENT", message="Vous ne pouvez pas accéder sur cette url, sera réserve à l’agent!")
     */
    public function depot(Request $request, EntityManagerInterface $manager,SoldesRepository $soldesRepository): Response
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        $user = $this->getUser();
        $depot = new Depot();

        $form = $this->createForm(DepotType::class, $depot);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $depot->setAgent($user);
            $manager->persist($depot);

            $solde = new Soldes();
            $montant_depot = $form->get("monDepot")->getData();
            $montant = $soldesRepository->findBy(['agent' => $user],['id' => 'DESC']);

                if(count($montant)){
                    $montant = $montant[0]->getMontant();
                } else{
                    $montant = 0;
                }

                if($montant < $montant_depot){
                    $this->addFlash("danger","Votre dépôt doit etre inferieur au solde");
    
                    return $this->redirectToRoute("montant_depot");
                }

                $montant -= $montant_depot ;
                
                $solde->setMontDepo($montant_depot);
                $solde->setAgent($user);
                $solde->setMontant($montant);
                $manager->persist($solde);

                $manager->flush();

            return $this->redirectToRoute('home');
        }

        
        return $this->render('FrontOffice/depot/index.html.twig',[
            'form'=>$form->createView()
        ]);
    }
}
