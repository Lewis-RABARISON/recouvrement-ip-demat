<?php

namespace App\Controller\BackOffice;

use App\Repository\CourseRepository;
use App\Repository\RechargementRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CourseController extends AbstractController
{
    /**
     * @Route("/mouvement", name="mouvement")
     * @IsGranted("ROLE_ADMIN", message="Vous ne pouvez pas accéder sur cette url, sera réserve à l’Administrateur!")
     */
    public function index(RechargementRepository $rechargementRepository): Response
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        $user = $this->getUser();
        // dd($rechargementRepository->CompteurRechargementAdmin());
        return $this->render('BackOffice/mouvement/index.html.twig', [
            'compteurs' => $rechargementRepository->CompteurRechargementAdmin(),
        ]);
    }
}
