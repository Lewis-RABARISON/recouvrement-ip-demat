<?php

namespace App\Controller\BackOffice;

use App\Repository\UserRepository;
use App\Repository\RechargementRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminRechargerController extends AbstractController
{
       /**
     * @Route("/rechargement-du-jour", name="recharger_jour")
     * @IsGranted("ROLE_ADMIN", message="Vous ne pouvez pas accéder sur cette url, sera réserve à l’Administrateur!")
     */
    public function recu(UserRepository $userRepository,
                          RechargementRepository $rechargementRepository): Response
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        return $this->render('BackOffice/admin_recharger/index.html.twig', [
            'users' => $userRepository->findAll(),
            'rechargements'=>$rechargementRepository->findAll()
        ]);
    }
}
