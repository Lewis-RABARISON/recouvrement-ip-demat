<?php

namespace App\Form;

use App\Entity\User;
use App\Form\ApplicationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends ApplicationType
{
   
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom',TextType::class, $this->getConfiguration("Nom", "Nom..."))
            ->add('prenom',TextType::class, $this->getConfiguration("Prénom(s)", "Prénom d'utilisateur..."))
            // ->add('nomComp')
            ->add('email',EmailType::class, $this->getConfiguration("email", "Adresse email..."))
            ->add('tel',TextType::class, $this->getConfiguration("Numéro téléphone", "Téléphone..."))
            ->add('Password',PasswordType::class, [
                'label' => 'Mot de passe'
            ])
            ->add('Confir_pwd',PasswordType::class, [
                'label' => 'Confirmer le mot de passe'
            ])
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    // 'Utilisateur' => "ROLE_USER",
                    'Agent'=> "ROLE_AGENT",
                    'Administrateur' => "ROLE_ADMIN"
                ],
                'expanded' => true,
                'multiple' => true,
                'label' => 'Rôles',
            ])
            ->add('picture',FileType::class,[
                'label' => 'Mettez un photo...',
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'class' => 'image-preview'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
