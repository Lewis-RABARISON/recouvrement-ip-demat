<?php

namespace App\Form;

use App\Entity\Depense;
use App\Form\ApplicationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class DepenseType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('motifs', TextType::class,[
                'label'=>'Motifs',
                'label' => 'Motifs'
            ])
            ->add('monDep', MoneyType::class,[
                'label'=>'Montant'
            ])
            ->add('preuve', FileType::class, $this->getConfiguration("Facture", "Mettre un photo..."),[
                'label'=>'Facture',
                // 'required' => false,
                'mapped' => false,
                // 'multiple'=>true,
                'attr' => [
                    'class' => 'image-preview'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Depense::class,
        ]);
    }
}
