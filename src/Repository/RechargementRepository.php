<?php

namespace App\Repository;

use App\Entity\Rechargement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class RechargementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rechargement::class);
    }

    public function CompteurRechargement($user)
    {
        return $this->createQueryBuilder('r')
                    ->select('COUNT (r) as compteur, r.date')
                    ->andWhere('r.agent=:user')
                    ->setParameter('user',$user)
                    ->groupBy('r.date')
                    ->getQuery()
                    ->getResult();
    }
    public function CompteurRechargementAdmin()
    {
        return $this->createQueryBuilder('r')
                    ->leftjoin('r.agent', 'agent')
                    ->select('COUNT (r) as compteur, r.date, agent.nom, agent.prenom, agent.numCom')
                    ->groupBy('r.agent, r.date')
                    ->getQuery()
                    ->getResult();
    }

    public function CompteJournalierRechargement()
    {
        $datetime = new \DateTime();
        $aujoudhui = $datetime->format('Y-m-d');
        $compte_journalier = 0;

        $result = $this->createQueryBuilder('r')
                        ->select('SUM(r.monRecu) as montant_recu, r.date')
                        ->andWhere('r.date=:aujoudhui')
                        ->setParameter('aujoudhui', $aujoudhui)
                        ->getQuery()
                        ->getResult();

        if(count($result) > 0){
            $compte_journalier = $result[0]['montant_recu'];
        }   
        
        return $compte_journalier;
    }

    public function CompteMensuelRechargement()
    {
        $datetime = new \DateTime();
        $compte_mensuel = 0;

        $annee_aujourdhui = $datetime->format('Y');
        $mois_aujourdhui = $datetime->format('m');

        $qb = $this->createQueryBuilder('r');
        $result = $qb
                        ->select('SUM(r.monRecu) as montant_recu')
                        /*->andWhere(
                            $qb->expr()->eq(
                                "DATE_FORMAT(:annee_aujourdhui, '%Y')",
                                "DATE_FORMAT(:mois_aujourdhui, '%m')")
                            )*/
                        /*->andWhere("DATE_FORMAT(r.date,'%Y') = :annee_aujourdhui")    
                        ->andWhere("DATE_FORMAT(r.date,'%m') = :mois_aujourdhui")   */ 
                        ->andWhere("YEAR(r.date) = :annee_aujourdhui")    
                        ->andWhere("MONTH(r.date) = :mois_aujourdhui")
                        ->setParameter('annee_aujourdhui', $annee_aujourdhui)
                        ->setParameter('mois_aujourdhui', $mois_aujourdhui)
                        ->getQuery()
                        ->getResult();

        if(count($result) > 0){
            $compte_mensuel = $result[0]['montant_recu'];
        }
        
        return $compte_mensuel;
    }
}
