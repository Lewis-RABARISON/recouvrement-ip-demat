<?php

namespace App\Repository;

use App\Entity\Depot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DepotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Depot::class);
    }

    public function CompteJournalierDeposer()
    {
        $datetime = new \DateTime();
        $aujoudhui = $datetime->format('Y-m-d');
        $compte_depose_journalier = 0;

        $result = $this->createQueryBuilder('r')
                        ->select('SUM(r.monDepot) as montant_depot')
                        ->andWhere('r.date=:aujoudhui')
                        ->setParameter('aujoudhui', $aujoudhui)
                        ->getQuery()
                        ->getResult();

        if(count($result) > 0){
            $compte_depose_journalier = $result[0]['montant_depot'];
        }   
        
        return $compte_depose_journalier;
    }

    public function CompteMensuelDepot()
    {
        $datetime = new \DateTime();
        $compte_mensuel = 0;

        $annee_aujourdhui = $datetime->format('Y');
        $mois_aujourdhui = $datetime->format('m');

        $qb = $this->createQueryBuilder('r');
        $result = $qb
                        ->select('SUM(r.monDepot) as montant_depot')
                        ->andWhere("DATE_FORMAT(r.date,'%Y') = :annee_aujourdhui")    
                        ->andWhere("DATE_FORMAT(r.date,'%m') = :mois_aujourdhui")
                        ->setParameter('annee_aujourdhui', $annee_aujourdhui)
                        ->setParameter('mois_aujourdhui', $mois_aujourdhui)
                        ->getQuery()
                        ->getResult();

        if(count($result) > 0){
            $compte_mensuel = $result[0]['montant_depot'];
        }
        
        return $compte_mensuel;
    }
}
