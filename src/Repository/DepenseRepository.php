<?php

namespace App\Repository;

use App\Entity\Depense;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Depense|null find($id, $lockMode = null, $lockVersion = null)
 * @method Depense|null findOneBy(array $criteria, array $orderBy = null)
 * @method Depense[]    findAll()
 * @method Depense[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DepenseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Depense::class);
    }

    public function CompteJournalierDepenser()
    {
        $datetime = new \DateTime();
        $aujoudhui = $datetime->format('Y-m-d');
        $compte_depenser_journalier = 0;

        $result = $this->createQueryBuilder('r')
                        ->select('SUM(r.monDep) as montant_depense')
                        ->andWhere('r.date=:aujoudhui')
                        ->setParameter('aujoudhui', $aujoudhui)
                        ->getQuery()
                        ->getResult();

        if(count($result) > 0){
            $compte_depenser_journalier = $result[0]['montant_depense'];
        }   
        
        return $compte_depenser_journalier;
    }

    public function CompteMensuelDepenser()
    {
        $datetime = new \DateTime();
        $compte_mensuel = 0;

        $annee_aujourdhui = $datetime->format('Y');
        $mois_aujourdhui = $datetime->format('m');

        $qb = $this->createQueryBuilder('r');
        $result = $qb
                        ->select('SUM(r.monDep) as montant_depense')
                        ->andWhere("DATE_FORMAT(r.date,'%Y') = :annee_aujourdhui")    
                        ->andWhere("DATE_FORMAT(r.date,'%m') = :mois_aujourdhui")
                        ->setParameter('annee_aujourdhui', $annee_aujourdhui)
                        ->setParameter('mois_aujourdhui', $mois_aujourdhui)
                        ->getQuery()
                        ->getResult();

        if(count($result) > 0){
            $compte_mensuel = $result[0]['montant_depense'];
        }
        
        return $compte_mensuel;
    }
}
